import React from "react";

export default function ViewPage({ match, data }) {
  console.log(data);
  console.log(match.params.id);
  var dataView = data.find((d) => d.ID == match.params.id);
  console.log(dataView);
  return (
    <div className="container">
      <h1>Article</h1>
      <div className="row">
        <div className="col-lg-4">
          <img src={dataView.IMAGE} alt="" className="w-100 p-3"/>
        </div>
        <div className="col-lg-8">
          <p>{dataView.TITLE}</p>
          <p>{dataView.DESCRIPTION}</p>
        </div>
      </div>
    </div>
  );
}
