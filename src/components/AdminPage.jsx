import React from "react";
import TableData from "./TableData";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function AdminPage(props) {
  if (props.loading) {
    return <center>Loading...</center>;
  }
  return (
    <div className="container">
      <center>
        <h1
          style={{
            fontFamily: "Lobster",
          }}
        >
          Article Management
        </h1>
      </center>
      <center>
        <Button
          variant="dark"
          as={Link}
          to="/add"
          style={{
            fontFamily: "Lobster",
          }}
        >
          Add New Article
        </Button>
      </center>
      <br />
      <TableData data={props.data} onDelete={props.onDelete} />
    </div>
  );
}
