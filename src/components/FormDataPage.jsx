import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import Axios from "axios";
import { Link } from "react-router-dom";
import Image from "../img/Default_Image_Thumbnail.png";
export default class FormDataPage extends Component {
  constructor() {
    super();
    this.state = {
      TITLE: "",
      DESCRIPTION: "",
      IMAGE: "",
      titleError: false,
      desError: false,

    };
  }

  changeHandler = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  onAdd = () => {
    if(this.state.TITLE === "" && this.state.DESCRIPTION === "" && this.state.IMAGE ===""){
        alert("Please Input Field !");
        return;
    }
    else{
      console.log(this.state);
      Axios.post("http://110.74.194.124:15011/v1/api/articles", this.state)
        .then((res) => {
          alert(res.data.MESSAGE);
        })
        .catch((err) => {
          alert(err);
        });
        
      this.props.onAdd();
    }
  };
  onImageChange = (event) => {
    console.log(event);
    if (event.target.files && event.target.files[0]) {
      this.setState({
        IMAGE: URL.createObjectURL(event.target.files[0]),
      });
    }
  };
  
  render() {
    return (
      <div className="container ">
        <h1>Add Article</h1>
        <div className="row">
          <div className="col-lg-8">
            <Form>
              <Form.Group>
                <Form.Label>Title</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter Title"
                  name="TITLE"
                  id="title"
                  value={this.state.name}
                  onChange={this.changeHandler}
                />
                 <label className="sms">{this.state.titleError?"!Validate title":null}</label>
              </Form.Group>

              <Form.Group>
                <Form.Label>Description</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter Description"
                  id="des"
                  name="DESCRIPTION"
                  value={this.state.desc}
                  onChange={this.changeHandler}
                />
                <label className="sms">{this.state.desError?"!Validate Description":null}</label>
              </Form.Group>
            </Form>
          </div>
          <div
            className="col-lg-4"
            style={{ border: "1px solid #ccc", width: "150px" }}
          >
            <input
              type="file"
              style={{ opacity: "0", width: "100%" }}
              onChange={(e) => this.onImageChange(e)}
              className="filetype"
              id="group_image"
            />
            <img
              src={this.state.IMAGE}
              alt=""
              style={{ width: "100%", height: "88%" }}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <Button
              variant="primary"
              type="button"
              onClick={() => this.onAdd()}
              as={Link}
              to="/"
            >
              Submit
            </Button>
          </div>
        </div>
      </div>
    );
  }
}
