import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import ReactDOM from "react-dom";


export default class Pagination extends Component {
    render() {

        const { postsPerPage, totalPosts, paginate, nextPage, previousPage, currentPage, pagination } = this.props;
        const pageNumber = [];

        for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
            pageNumber.push(i);
        }

        return (
            <nav className={pagination === totalPosts ? 'none' : 'show'}>
                <ul className="pagination justify-content-center">

                    <li className="page-item">
                        <Button variant="primary" disabled={totalPosts < 4 || currentPage === 1} onClick={() => previousPage()}>Previous</Button>
                    </li>

                    {pageNumber.map((number) => (
                        <li className={currentPage === number ? 'active page-item' : 'page-item'} key={number}>
                            <a href="#" className="page-link " onClick={() => paginate(number)}>{number}</a>
                        </li>
                    ))}

                    <li className="page-item">
                        <Button variant="primary" disabled={totalPosts < 4 || currentPage * 3 >= totalPosts} onClick={() => nextPage()}>Next</Button>
                    </li>
                </ul>
            </nav>
        )
    }
}


