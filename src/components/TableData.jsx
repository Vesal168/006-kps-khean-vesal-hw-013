import React, { Component } from "react";
import { Table, Button } from "react-bootstrap";
import Axios from "axios";
import { Link } from "react-router-dom";

class TableData extends Component {
  constructor(props) {
    super(props);
    console.log(this.props.data);
  }

  onDelete = (deleteID) => {
    if (window.confirm("Are you want to delete ?")) {
      Axios.delete(`http://110.74.194.124:15011/v1/api/articles/${deleteID}`)
        .then((res) => {
          alert(res.data.MESSAGE);
          console.log(res);
        })
        .catch((err) => {
          alert(err);
        });
      this.props.onDelete(deleteID);
    }
  };
  convertDate(dateStr) {
    var dateString = dateStr;
    var year = dateString.substring(0, 4);
    var month = dateString.substring(4, 6);
    var day = dateString.substring(6, 8);
    var date = year + "-" + month + "-" + day;
    return date;
  }
  render() {
    const table = this.props.data.map((d) => (
      <tr key={d.ID}>
        <td>{d.ID}</td>
        <td>{d.TITLE}</td>
        <td>{d.DESCRIPTION}</td>
        <td>{this.convertDate(d.CREATED_DATE)}</td>
        <td>
          <img src={d.IMAGE} alt="" style={{ width: "80px", height: "80px" }} />
        </td>
        <td>
          <div style={{ width: "90%", margin: "auto" }}>
            <Button
              as={Link}
              to={`/view/${d.ID}`}
              variant="primary"
              style={{ marginTop: "5px" }}
            >
              View
            </Button>{" "}
            <Button
              variant="warning"
              style={{ marginTop: "5px" }}
              as={Link}
              to={`/update/${d.ID}`}
            >
              Edit
            </Button>{" "}
            <Button
              variant="danger"
              as={Link}
              to="/"
              style={{ marginTop: "5px" }}
              onClick={() => this.onDelete(d.ID)}
            >
              Delete
            </Button>
          </div>
        </td>
      </tr>
    ));
    return (
      <div>
        <Table striped bordered hover>
          <thead className="thead-dark" style={{ fontFamily: "Roboto Mono" }}>
            <tr className="text-center">
              <th>ID</th>
              <th style={{ width: "25%" }}>Title</th>
              <th style={{ width: "30%" }}>Description</th>
              <th style={{ width: "15%" }}>Create Date</th>
              <th>Image</th>
              <th style={{ width: "100%" }}>Action</th>
            </tr>
          </thead>
          <tbody>{table}</tbody>
        </Table>
      </div>
    );
  }
}

export default TableData;
