import React, { Component } from "react";
import Axios from "axios";
import AdminPage from "./components/AdminPage";
import NavbarCom from "./components/NavbarCom";
import Pagination from './components/pagination/Pagination'
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import FormDataPage from "./components/FormDataPage";
import ViewPage from "./components/ViewPage"
import Update from "./components/Update";


export default class App extends Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      data: [],
      currentPage: 1,
      postsPerPage: 3,
      pagination: 0,
      isUpdate: false,
      search: "",
      img: "",
      isEditProduct: false,
      TITLE: "",
      DESCRIPTION: "",
      titleError: false,
      desError: false,

    };
  }
  convertDate(dateStr) {
    var dateString = dateStr;
    var year = dateString.substring(0, 4);
    var month = dateString.substring(4, 6);
    var day = dateString.substring(6, 8);
    var date = year + "-" + month + "-" + day;
    return date;
  }
  handleSearch = (e) => {
    this.setState({
      search: e.target.value.substr(0, 20)
    })
  }
  componentDidMount() {
    Axios.get("http://110.74.194.124:15011/v1/api/articles")
      .then((res) =>
        this.setState({
          loading: false,
          data: res.data.DATA,
        })
      )
      .catch((err) => {
        alert(err)
      });
    console.log(this.state.data);
  }
  onUpdate = () => {
    this.setState({
      isUpdate: true,
    })
  }
  onDelete = (deleteID) => {
    let datas = this.state.data.filter((data) => data.ID !== deleteID);
    this.setState({
      data: datas,
    })
  }
  
  onAdd = () => {
    
    Axios.get("http://110.74.194.124:15011/v1/api/articles")
      .then((res) =>
        this.setState({
          loading: false,
          data: res.data.DATA,
        })
      )
      .catch((err) => {
        alert(err);
      });
      
    console.log(this.state.data)

  }
  render() {

    const { currentPage, postsPerPage, data, pagination } = this.state;
    const indexOfLastPost = currentPage * postsPerPage;
    const indexOfFirstPage = indexOfLastPost - postsPerPage;
    const currentPosts = data.slice(indexOfFirstPage, indexOfLastPost);
    const paginate = pageNumber => this.setState({ currentPage: pageNumber });

    let nextPage;
    let previousPage;

    nextPage = () => this.setState({ currentPage: (currentPage + 1) });
    previousPage = () => this.setState({ currentPage: (currentPage - 1) });

    return (
      <div>

        <Router>
          <NavbarCom/>
          <Switch>
            <Route path="/add" render={() => <FormDataPage dataList={currentPosts} onAdd={this.onAdd} />} />
            <Route path={"/view/:id"} render={(props) => <ViewPage  {...props} data={this.state.data} />} />
            <Route
              path={"/update/:id"}
              render={(props) => (
                <Update
                  {...props}
                  data={this.state.data}
                />
              )}
            />
            <AdminPage  data={this.state.data} loading={this.state.loading} onDelete={this.onDelete} onAdd={this.onAdd} onUpdate={this.onUpdate} />
          </Switch>
        </Router>

        <Pagination postsPerPage={postsPerPage} totalPosts={data.length}
          paginate={paginate} nextPage={nextPage} previousPage={previousPage} currentPage={currentPage} pagination={pagination} />

      </div>
    );
  }
}
